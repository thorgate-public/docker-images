variables:
  DOCKER_DRIVER: overlay2

services:
  - docker:dind

stages:
  - base
  - build-image-1
  - build-image-2
  - build-image-3
  - test
  - release

builder:
  stage: base
  image: docker:latest
  services:
    - docker:dind
  variables:
    BUILD_IMAGE_NAME: "$CI_REGISTRY_IMAGE/builder:latest"
  script:
    - 'echo ${CI_JOB_TOKEN} | docker login --password-stdin -u $CI_REGISTRY_USER $CI_REGISTRY'
    - docker build --tag "$BUILD_IMAGE_NAME" .
    - docker push "$BUILD_IMAGE_NAME"


###############################
#       BUILD STAGE           #
###############################

.build: &build
  stage: build-image-1
  image: $CI_REGISTRY_IMAGE/builder:latest
  needs:
    - builder
  script:
    - 'echo ${CI_JOB_TOKEN} | docker login --password-stdin -u $CI_REGISTRY_USER $CI_REGISTRY'
    - tg-builder --build-image $BUILD_IMAGE


build-python-3.6:
  <<: *build
  variables: &python36
    BUILD_IMAGE: "python"
    BUILD_ARG_PYTHON_VERSION: "3.6"
    BUILD_VERSION_ARG: "BUILD_ARG_PYTHON_VERSION"

build-python-tox-3.6:
  <<: *build
  variables: &python36tox
    BUILD_IMAGE: "python-tox"
    BUILD_ARG_PYTHON_VERSION: "3.6"
    BUILD_ARG_PYTHON_TOX_FACTOR: "py36"
    BUILD_VERSION_ARG: "BUILD_ARG_PYTHON_VERSION"

build-python-tox-3.7:
  <<: *build
  variables: &python37tox
    <<: *python36tox
    BUILD_ARG_PYTHON_TOX_FACTOR: "py37"
    BUILD_ARG_PYTHON_VERSION: "3.7"

build-python-tox-3.8:
  <<: *build
  variables: &python38tox
    <<: *python36tox
    BUILD_ARG_PYTHON_TOX_FACTOR: "py38"
    BUILD_ARG_PYTHON_VERSION: "3.8"

build-python-tox-3.9:
  <<: *build
  variables: &python39tox
    <<: *python36tox
    BUILD_ARG_PYTHON_TOX_FACTOR: "py39"
    BUILD_ARG_PYTHON_VERSION: "3.9"

build-python-tox-3.10:
  <<: *build
  variables: &python310tox
    <<: *python36tox
    BUILD_ARG_PYTHON_TOX_FACTOR: "py310"
    BUILD_ARG_PYTHON_VERSION: "3.10"


build-python-tox-3.11:
  <<: *build
  variables: &python311tox
    <<: *python36tox
    BUILD_ARG_PYTHON_TOX_FACTOR: "py311"
    BUILD_ARG_PYTHON_VERSION: "3.11"


build-python-tox-3.12:
  <<: *build
  variables: &python312tox
    <<: *python36tox
    BUILD_ARG_PYTHON_TOX_FACTOR: "py312"
    BUILD_ARG_PYTHON_VERSION: "3.12"

build-python-3.6-dev:
  <<: *build
  stage: build-image-2
  needs:
    - builder
    - build-python-3.6
  variables: &python36dev
    BUILD_IMAGE: "python-dev"
    BUILD_ARG_PYTHON_VERSION: "3.6"
    BUILD_ARG_PYTHON_IMAGE_VERSION: "$CI_COMMIT_SHA"
    BUILD_VERSION_ARG: "BUILD_ARG_PYTHON_VERSION"

build-msbinencoder:
  <<: *build
  variables: &msbinencoder
    BUILD_IMAGE: "msbinencoder"

build-docker-eveli:
  <<: *build
  variables: &docker-eveli
    BUILD_IMAGE: "eveli"


###############################
#        TEST STAGE           #
###############################

.test: &test
  stage: test
  services:
    - name: docker:dind
      alias: docker

test-docker-eveli:
  <<: *test
  image: docker:latest
  variables:
    IMAGE_NAME: "$CI_REGISTRY_IMAGE/eveli:$CI_COMMIT_SHA"
    POSTGRES_PASSWORD: test
  before_script:
    - apk add --no-cache jq
    - docker pull nginx:latest
    - docker pull postgres:12
    - docker pull $IMAGE_NAME
  script:
    - docker run --name nginx --detach nginx:latest
    - docker run --name postgres-12 --detach -e POSTGRES_PASSWORD=test postgres:12
    - docker run --name eveli-daemon --detach -v "/var/run/docker.sock:/var/run/docker.sock" "$IMAGE_NAME" python /usr/bin/eveli.py --console-logging
    - docker run nginx sleep 1
    - |
      [[ "$(docker inspect eveli-daemon | jq '.[0]|.State.Status' -r)" == "running" ]] && echo "OK" || exit 1
    - docker exec nginx bash -c "echo FAIL_LINE >> /etc/nginx/nginx.conf"
    - docker run nginx sleep 1
    - docker logs eveli-daemon
    - |
      [[ "$(docker inspect eveli-daemon | jq '.[0]|.State.Status' -r)" == "running" ]] && echo "OK" || exit 1
  after_script:
    - docker stop nginx
    - docker stop postgres-12
    - docker stop eveli-daemon


###############################
# RELEASE STAGE :: LATEST TAG #
###############################

.release: &release
  stage: release
  image: $CI_REGISTRY_IMAGE/builder:latest
  script:
    - 'echo ${CI_JOB_TOKEN} | docker login --password-stdin -u $CI_REGISTRY_USER $CI_REGISTRY'
    - tg-builder --push-latest $BUILD_IMAGE
  only:
    - master
    - schedules

release-python-3.6:
  <<: *release
  needs:
    - builder
    - build-python-3.6
  variables:
    <<: *python36

release-python-3.6-dev:
  <<: *release
  needs:
    - builder
    - build-python-3.6-dev
  variables:
    <<: *python36dev

release-python-tox-3.6:
  <<: *release
  needs:
    - builder
    - build-python-tox-3.6
  variables:
    <<: *python36tox

release-python-tox-3.7:
  <<: *release
  needs:
    - builder
    - build-python-tox-3.7
  variables:
    <<: *python37tox

release-python-tox-3.8:
  <<: *release
  needs:
    - builder
    - build-python-tox-3.8
  variables:
    <<: *python38tox

release-python-tox-3.9:
  <<: *release
  needs:
    - builder
    - build-python-tox-3.9
  variables:
    <<: *python39tox

release-python-tox-3.10:
  <<: *release
  needs:
    - builder
    - build-python-tox-3.10
  variables:
    <<: *python310tox

release-python-tox-3.11:
  <<: *release
  needs:
    - builder
    - build-python-tox-3.11
  variables:
    <<: *python311tox

release-python-tox-3.12:
  <<: *release
  needs:
    - builder
    - build-python-tox-3.12
  variables:
    <<: *python312tox

release-msbinencoder:
  <<: *release
  needs:
    - builder
    - build-msbinencoder
  variables:
    <<: *msbinencoder

release-docker-eveli:
  <<: *release
  needs:
    - builder
    - build-docker-eveli
    - test-docker-eveli
  variables:
    <<: *docker-eveli
