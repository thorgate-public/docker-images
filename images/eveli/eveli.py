import logging.config
import re

import click
import docker
import docker.errors
import docker.models.containers
import docker.models.networks
from docker import DockerClient


def setup_logging(console_logging: bool):
    logging_config = {
        "version": 1,
        "formatters": {
            "default": {
                "format": "%(asctime)s [%(levelname)s] %(name)s:%(lineno)d %(funcName)s - %(message)s"
            },
        },
        "handlers": {
            "log": {
                "class": "logging.handlers.WatchedFileHandler",
                "filename": "/var/log/docker-eveli/info.log",
                "formatter": "default",
            },
        },
        "loggers": {
            "": {
                "handlers": ["log"],
                "level": "INFO",
            }
        },
    }

    if console_logging:
        logging_config["handlers"] = {
            "log": {"class": "logging.StreamHandler", "formatter": "default"},
        }

    logging.config.dictConfig(logging_config)


def get_image_name(container_from) -> str:
    """ Extracts the image name (without tag) from container's 'from' field. """
    return container_from.rsplit(":", 1)[0]


def get_compose_projects(client: DockerClient) -> list[str]:
    """Returns list of names of active Docker Compose projects"""
    projects = set()
    for container in client.containers.list():
        project = container.labels.get("com.docker.compose.project")
        if project:
            projects.add(project)

    return list(projects)


def get_postgres_container(client: DockerClient):
    regex = re.compile(r"postgres-\d+\.?\d+", flags=re.I)

    for container in client.containers.list():
        if regex.match(container.name):
            return container.name


def try_connect_container_to_network(client: DockerClient, container, network_name):
    try:
        network: docker.models.networks.Network = client.networks.get(network_name)
        network.connect(container)
    except docker.errors.APIError:
        pass


def ensure_networks(client: DockerClient):
    projects = get_compose_projects(client)
    logging.info("ensure_networks(): docker-compose projects found: %s", projects)

    postgres_container = get_postgres_container(client)

    for project in projects:
        try_connect_container_to_network(client, "nginx", project + "_nginx")

        if postgres_container:
            try_connect_container_to_network(
                client, postgres_container, project + "_postgres"
            )


def try_nginx_reload(client: DockerClient):
    try:
        nginx_container: docker.models.containers.Container = client.containers.get("nginx")
        exit_code, output = nginx_container.exec_run(cmd="nginx -s reload")

        if exit_code != 0:
            raise RuntimeError(output)
    except docker.errors.APIError:
        pass


def handler_nginx_reload(client: DockerClient, event: dict):
    """Sends reload signal to nginx whenever a container's IP might have changed"""

    required_keys = ["Type", "Action", "from"]
    actions = ["start", "stop", "pause", "unpause", "kill"]

    if (
        not all(k in event for k in required_keys)
        or event["Action"] not in actions
    ):
        return

    logging.info(
        "handler_nginx_reload() %s %s %s", event["Action"], event["Type"], event["from"]
    )

    try_nginx_reload(client)


def handler_compose_networks(client: DockerClient, event: dict):
    """Ensures that nginx and postgres are in corresponding networks of each docker-compose project

    They need to be re-added e.g. when nginx/postgres containers are restarted.
    """

    required_keys = ["Type", "Action", "from"]
    actions = ["start", "unpause"]
    image_names = ["nginx", "postgres"]

    if not all(k in event for k in required_keys) or event["Action"] not in actions:
        return

    if get_image_name(event["from"]) not in image_names:
        return

    logging.info(
        "handler_compose_networks() %s %s %s",
        event["Action"],
        event["Type"],
        event["from"],
    )
    ensure_networks(client)


@click.command()
@click.option("--from-env", "docker_from_env", flag_value=True, default=False)
@click.option("--console-logging", "console_logging", flag_value=True, default=False)
def listen(docker_from_env, console_logging):
    setup_logging(console_logging)

    if docker_from_env:
        client = docker.from_env()
    else:
        client = DockerClient(base_url="unix:///var/run/docker.sock")

    logging.info("Eveli is alive!")

    # Just in case, run the networks-fixer at startup as well
    ensure_networks(client)

    # Trigger nginx reload at start up - in case it was missed
    try_nginx_reload(client)

    # Could use decorators to register the event handlers when we get more of them
    event_handlers = [
        handler_nginx_reload,
        handler_compose_networks,
    ]

    for event in client.events(decode=True):
        for handler in event_handlers:
            try:
                handler(client, event)
            except:
                logging.exception("Event handler %s failed", handler.__name__)


if __name__ == "__main__":
    listen()
