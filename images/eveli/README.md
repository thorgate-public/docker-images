# Docker-eveli

Eveli is Docker event-listener that handles reloading nginx container when any container is started or stopped.

This is required because Nginx caches upstream IPs and when container is restarted it's IP will change.
It also ensures nginx and postgres container on the server are connected to respective networks for each compose project.

Eveli is designed to work together with Django Project-Template (DPT) and is required when running multiple DPT projects on the same server.


### Running eveli

Expected containers on the server: 
- `nginx`
- `postgres-<major-version>`

Mount following volumes:
- `/var/run/docker.sock:/var/run/docker.sock`
- `/var/log/docker-eveli:/var/log/docker-eveli`
