# Dotnet MSBIN encoder/decoder

This script uses `dotnet-script` tool, and the script originally
written by Brian Holyfield, Gotham Digital Science (labs@gdssecurity.com),
(see msbin.cs for detailed info).

    dotnet-script msbin.cs -- encode "<IamSoapXML data......
    dotnet-script msbin.cs -- decode "IAm BASE64 data ==......
    
For uconvenience, a simple python wrapper runs a WSGI app that accepts XML (or base64 encoded msbin)
over HTTP post requests and returns there encoded (or decoded) result.

If there is an error, HTTP service responds with HTTP 400 and error description in the response body 
(HTTP 200 is used normally).

## Example CURL to test this out

Assuming port 9000 is exposed
```
curl -X POST -d 'QQRzb2FwCEVudmVsb3BlCQRzb2FwJ2h0dHA6Ly93d3cudzMub3JnLzIwMDMvMDUvc29hcC1lbnZlbG9wZQkDdGVtE2h0dHA6Ly90ZW1wdXJpLm9yZy9BBHNvYXAGSGVhZGVyAUEEc29hcARCb2R5QQN0ZW0HR2V0QXV0aAEBAQ==' http://127.0.0.1:9000/decode -i
```
