import bottle
import subprocess
import traceback


import logging
logging.basicConfig(level=logging.INFO)


application = bottle.Bottle()


def execute_msbin(*arguments):
    try:
        return subprocess.check_output(["/app/msbin", *arguments])
    except subprocess.CalledProcessError as e:
        logging.exception("Failed to process request. Arguments: %r", arguments)
        return bottle.HTTPResponse(
            status=400,
            body=f"{e.output}\n\n{traceback.format_exc()}",
        )


@application.post("/encode")
def do_encode():
    content = bottle.request.body.read().decode().replace("\n", "")
    logging.debug("Encoding:\n%s", content)
    return execute_msbin("encode", content)


@application.post("/decode")
def do_decode():
    content = bottle.request.body.read().decode().replace("\n", "")
    logging.debug("Decoding:\n%s", content)
    return execute_msbin("decode", content)


@application.route("/")
def index():
    return "MSBIN encoder and decoder. Post your data to /encode (XML) or /decode (base64 encoded msbin)."
