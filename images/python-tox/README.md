# python-tox images

Intended use - in CI for python packages that require testing with tox in multiple
python environments.

Example `.gitlab-ci.yaml` file:

```yaml
stages:
    - test

test38:
    stage: test
    image: "registry.gitlab.com/thorgate-public/docker-images/python-tox-3.8:latest"
    coverage: '/^TOTAL.*\s+(\d+\%)$/'
    except:
        variables:
            - $CI_COMMIT_MESSAGE =~ /tests skip/
            - $CI_COMMIT_MESSAGE =~ /test skip/
            - $CI_COMMIT_MESSAGE =~ /\[no tests?\]/


test39:
    stage: test
    image: "registry.gitlab.com/thorgate-public/docker-images/python-tox-3.9:latest"
    except:
        variables:
            - $CI_COMMIT_MESSAGE =~ /tests skip/
            - $CI_COMMIT_MESSAGE =~ /test skip/
            - $CI_COMMIT_MESSAGE =~ /\[no tests?\]/
```

Tox will automatically only run relevant environment (for python version in particular container).

tox.ini for inspiration (preferably, should be in pyproject.toml under `[tool.tox] legacy_tox_ini`)

```
[tox]
isolated_build = true
envlist = py{38,39}-django{22,30,31}

[testenv]
allowlist_externals =
    make
    poetry
deps =
    django22: Django>=2.2,<3.0
    django30: Django>=3.0,<3.1
    django31: Django>=3.1,<3.2
commands =
    poetry install
    make test
```