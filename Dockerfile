FROM docker:latest

# Install Dependencies
RUN apk add -U curl tar gzip bash python3 python3-dev py3-six py3-docker-py

COPY src/ build/

RUN ln -s /build/bin/* /usr/local/bin/
